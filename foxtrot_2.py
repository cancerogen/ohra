#!/usr/bin/env python
# -*- coding: utf-8 -*-
import myirclib
from random import randint, choice
import threading
import time
import urllib
import simplejson
import foxtrot_conf
import sys
# --------------------------
config = foxtrot_conf.config
GLOB_COOLDOWN = foxtrot_conf.GLOB_COOLDOWN
prefix = foxtrot_conf.prefix
patch_to_greeting = foxtrot_conf.patch_to_greeting
owner = foxtrot_conf.owner
owner_comands = foxtrot_conf.owner_comands
load_commands = foxtrot_conf.load_commands
command_description = foxtrot_conf.command_description
if foxtrot_conf.DEBUG: myirclib.DEBUG = True
else: myirclib.DEBUG = False
list_greeting = [] 
#--------------------------
with open(patch_to_greeting,'r') as f1:
	for lines in f1.readlines():
		list_greeting.append(lines)#.decode('utf-8').encode('cp1251'))
	f1.close()
#--------------------------
def handling_commands(sock,msg):
	'Costeal here'
	if len(msg[3]) < 2:
		return None
	msg[3] = msg[3][1:]
	message = ' '.join(msg[3:])
	print message
	senderData = msg[0].split("!")
	senderData = senderData[0].replace(":","")
	chan = msg[2]
	if msg[2] == config['chan']:
		if msg[3][0] == prefix or msg[3] == '!help':
			words = msg[3][1:]
			if  words == 'hi':
				sock.send_to_chan(config['chan'],' hello')
			elif words == 'help':
				handler_help(sock, senderData)
			if words == 'google':
				handler_google(sock, chan, senderData, message)
			elif words == 'roll':
				handler_roll(sock, chan, senderData, message)
			elif words == 'greeting':
				handler_greeting(sock, chan, senderData)
			#elif _words[0] == 'translate' or _words[0] == 'перевод':
			#	handler_translate(conn,event)
			#else: conn.privmsg(event.target()," Нет такой команды")
	elif msg[2] == sock.nick:
		if msg[3][0] == prefix:
			words = msg[3][1:]
			if words == 'quit' and senderData == owner:
				sock.die()
				print 'Goodbye main'
				sys.exit()
#
def handling_events(sock,msg):
	senderData = msg[0].split("!")
	senderData = senderData[0].replace(":","")
	if len(msg) > 2:
		chan = msg[2][1:]
		if msg[1] == 'JOIN':
			wellcome(sock,chan,senderData)
#------------------------------------------------
def process_conn(config):
	IRC = myirclib.Client(config)
	IRC.login()
	IRC.start()
	time.sleep(3)
	IRC.joinChannel(IRC.channel)
	while 1:
		if IRC.que:
			#if DEBUG: print 'in que'
			handling_commands(IRC,IRC.que.pop(0))
		elif IRC.events:
			handling_events(IRC,IRC.events.pop(0))
		else:
			time.sleep(GLOB_COOLDOWN)
#
def handler_help(sock,SendTo): #commands listing command
	for comm in load_commands:
		if comm not in owner_comands:
			mess = command_description[comm]
			sock.sendData("PRIVMSG %s :%s" % (SendTo, mess))
			time.sleep(GLOB_COOLDOWN)
#
def handler_google(sock, chan, senderData, message):
	def gsearch(text):
		query = urllib.urlencode({'q' : text})
		url = u'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' \
		% (query)
		try:
			search_results = urllib.urlopen(url)
			json = simplejson.loads(search_results.read())
			results = json['responseData']['results']
			return results
		except: return None
	if len(message.split(' ')) > 1:
		params = message.split(' ')[1:]
		params = ' '.join(i for i in params)
		print params
		resp = gsearch(params)
		if resp:
			mess = '\"05%s01\"  ' % str(resp[0]['title'].encode('utf-8')).replace('<b>','').replace('</b>','05')
			content = str(resp[0]['content'].encode('utf-8')).replace('<b>','').replace('</b>','')
			mess = mess + content + '  02%s' % str(resp[0]['url'].encode('utf-8'))
			sock.send_to_chan(chan,mess)
		else:
			'Ошибка в запросе %s' % message
			return None
	else: return None
#
def handler_roll(sock, chan, senderData, message):
	_words = message.split(' ')
	if len(_words) == 2:
		try:
			if int(_words[1]) > 1 and int(_words[1]) < 101:
				number = randint(0,int(_words[1]))
			else:
				number = randint(0,1)
		except:
			print senderData,": Неправильные аргументы"
			return None
	else: number = randint(0,1)
	sock.send_to_chan(chan, str(number))

#
def handler_greeting(sock, chan, senderData):
	greet = choice(list_greeting)
	sock.sendData("PRIVMSG %s :%s" % (chan, greet))
#
def wellcome(sock,chan,senderData):
	greet = choice(list_greeting)
	msg = "05,01%s: 09,01%s" % ( senderData, greet)
	sock.send_to_chan(chan,msg)
#---------------------------
if __name__ == "__main__":
	process_conn(config)
