#!/usr/bin/env python
# -*- coding: utf-8 -*-
config = {
	'nick':'foxtrot',
	'ident':'Bot',
	'real':'lolka-san',
	'server':'irc.freenode.net',
	'port':6667,
	'chan':'#botroom',
	'encoding':''}

DEBUG = True # Send into console raw data
GLOB_COOLDOWN = 1 # Global cooldown for posting
prefix = '@' # Command prefix
patch_to_greeting = './badwords.txt'
owner = 'defdefdef' # Owner of Bot
owner_comands = [] # Команды доступные только овнеру
load_commands = ['help','roll','google','greeting'] # Загружаемые команды

# There description should send if you send HELP to bot
command_description = {'help':'%shelp - Справка по командам' % prefix,\
	'roll': '%sroll(!ролл) [2-100] - рулеточка, без параметров выдает 0 или 1' % prefix,\
	'google':'%sgoogle мокрые писечки - первая ссылка в гугле по запросу' % prefix,\
	'greeting':'%sgreeting - случайное приветствие' % prefix}
