#!/usr/bin/env python
# -*- coding: utf-8 -*-
import socket, time, sys
import threading
import logging
# if you want use SOCKS PROXY - uncommend this lines
#import socks
'''
SocksiPy - Python SOCKS module.
Version 1.00
Copyright 2006 Dan-Haim. All rights reserved.
http://socksipy.sourceforge.net/

Take some code here:
https://github.com/gentoomen/Pymn/blob/master/connection.py
'''
# Example of using socks lib
#socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,"12.34.56.78",1234)
#socket.socket = socks.socksocket
#sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#----------------------------------------------
DEBUG = False # Logging debug only messages
LOGRAW = False # Logging raw data
CONSOLE = False
#who_list = '' # List of users on channel
# Path to log file
logging.basicConfig(filename='./log.txt',level=logging.DEBUG)
# ----------------------------------------------
class Client(threading.Thread):
	def __init__(self, config, socks_adrr = None):		
		threading.Thread.__init__(self)
		self.setDaemon(True)
		# socks_adrr = ["host",port]
		self.host = config['server']
		self.port = config['port']
		self.channel = config['chan'] # Channel with botmaster
		self.nick = config['nick']
		self._ident = config['ident']
		self.real = config['real']
		self.passwd = ''
		self.encoding = config['encoding']
		#self.que  Queue of commands (PRIVMSG)
		#self.events  Queue of other events (JOIN, PART, WHO)
		self.que, self.events = [],[]
		#self.first_ping = False
		# if use SOCKS PROXY then initialization socks settings
		if socks_adrr:
			self.socks_host = socks_adrr[0]
			self.socks_port = socks_adrr[1]
			socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5,self.socks_host,self.socks_port)
			socket.socket = socks.socksocket
		else: pass
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((self.host,self.port))
	#
	def run(self): self.handling_ping()
	#
	def irc_conn(self):
		self.sock.connect((self.host,self.port))
		print "Attempting to connect to " + self.host
	#
	def sendData(self,command):
		self.sock.send(command + '\n')
		if LOGRAW and command != 'PONG': logging.info('<----'+ command)
		if CONSOLE: print '<----' + command
	#
	def send_to_chan(self,chan,message):
		self.sendData("PRIVMSG %s :%s" % (chan,message))
	#
	def pong(self,msg):
		if msg[0] == "PING":
			self.sendData("PONG %s" % msg[1])
			#if not self.first_ping: self.first_ping = True
		elif msg[1] == "PRIVMSG":
			self.que.append(msg)
			if DEBUG: print 'drop to que'+ self.channel
		else:
			self.events.append(msg)
	#
	def joinChannel(self, channel):
		print "Joining to " + channel
		self.sendData( "JOIN %s" % channel)
	#
	def login(self):
		print "Login as " + self.nick
		self.sendData( "NICK %s" % self.nick)
		# (username, hostname, servername, realname))
		self.sendData( "USER %s %s %s :%s" % (self._ident, '8', '*', self.real))
		'if you want use register nick on server uncomment this lines'
		#if self.passwd != "":
		#	self.sendData("PRIVMSG NickServ :ID %s" % (self.passwd))
	#
	def handling_ping(self):
		while 1:
			buffer = self.sock.recv(1024)
			if not buffer: print 'No data', sys.exit()
			if CONSOLE: print "---->" + buffer
			msg = buffer.split('\r\n')
			if msg[-1] == '': del msg[-1]
			#if LOGRAW and msg[0] != "PING":
			#	logging.info("---->" + buffer)
			try:
				# TODO here
				if len(msg) > 1:
					for i in range(len(msg)):
						#print i,'===', msg[i]
						_msg = msg[i].split()
						self.pong(_msg)
				else:
					_msg = msg[0].split()
					if len(_msg) < 2:  print 'Something wrong'# dissconnect
					self.pong(_msg)
					#print '0 === ', msg[0]
				time.sleep(0.1)
			except: pass
			#Exception,error:
			#	if DEBUG:
			#		logging.debug(error)
			#		logging.debug(' ^ in handling_ping')
	#
	def die(self):
		self.sendData("QUIT :Bye")
		self.sock.close()
		print "Goodbye client"
		# TODO make stop while 1 in handling_ping and kill thread
#
