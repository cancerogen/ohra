#!/usr/bin/env python
# -*- coding: utf-8 -*-
import myirclib
import time
import sys
import logging
#--------------------------
config1 = {
			'nick':'Watching',
			'ident':'Bot',
			'real':'lolka-san',
			'server':'irc.freenode.net',
			'port':6667,
			'chan':'#botroom',
			'encoding':'utf8'}

config2 = {
			'nick':'Watching2',
			'ident':'Bot',
			'real':'lolka-san',
			'server':'irc.freenode.net',
			'port':6667,
			'chan':'#botroom1',
			'encoding':'utf8'}
#
help_message = "This is ircgate bot. Deliveped by defdefdef. \
Based on ohra lib. https://bitbucket.org/defdefdef/ohra/ "
#--------------------------
# Adress of your socks proxy
# If you want to use it, then input socks host as a string and port as a number
# like a ["12.34.56.78",1234]
socks = [] # Disabled by default
who_list = ''
DEBUG = True
GLOB_COOLDOWN = 1
prefix = '@'
owner = 'defdefdef'
#--------------------------
if DEBUG: myirclib.DEBUG = True
else: myirclib.DEBUG = False
#------------------------------------------------
def handler_help(sock,SendTo): # !help
	sock.sendData("PRIVMSG %s :%s" % (SendTo, help_message))
	time.sleep(GLOB_COOLDOWN)
#
def process_one(config):
	if socks: IRC = myirclib.Client(config,socks_adrr = socks)
	else: IRC = myirclib.Client(config)
	IRC.login()
	IRC.start()
	time.sleep(3)
	IRC.joinChannel(IRC.channel)
	return IRC
#
def simple_gate(config1,config2):
	"""
	Simple two-way gate, for 2 channels are hardcode relink
	"""
	IRC1 = process_one(config1) # First client
	IRC2 = process_one(config2) # Second client
	#botnet = [IRC1,IRC2]
	while 1:
		if IRC1.que:
			repost(IRC1,IRC2,IRC1.que.pop(0))
		if IRC2.que:
			repost(IRC2,IRC1,IRC2.que.pop(0))
		if not IRC1.que and not IRC2.que:
			time.sleep(GLOB_COOLDOWN)
#
def repost(from_sock,to_sock,msg):
	print 'get %s' % from_sock.channel
	if len(msg[3]) == 0: return None
	msg[3] = msg[3][1:]
	message = ' '.join(msg[3:])
	senderData = msg[0].split("!")
	senderData = senderData[0].replace(":","")
	chan = msg[2]
	# Only messages with a given channel
	if msg[2] == from_sock.channel:
		if not msg[3] == '!help':
			#TODO: if senderData not in ignore list
			f_enc = from_sock.encoding
			t_enc = to_sock.encoding
			# If the field "encoding" in the configuration Bot filled
			if f_enc and t_enc:
				# And they are different
				if f_enc != t_enc:
					# decoding is very dangerous
					try:
						message = message.decode(f_enc).encode(t_enc) # Декодируем
					except Exception,error:
						if DEBUG:
							logging.debug(error)
							logging.debug(' ^ in repost')
				else: pass # If the same is no decode
			else: pass # If not specified, it is also not decode
			mess =  "02[%s]05<%s>:  %s" % (chan,senderData, message)
			to_sock.send_to_chan(sock.channel, mess)
		else: handler_help(from_sock, senderData)
#
def main(config1,config2):
	#TODO
	# parse console options
	#if sys.argv[] ....
	simple_gate(config1,config2)
#---------------------------
if __name__ == "__main__":
	try:
		main(config1,config2)
	except Exception,error:
				if DEBUG:
					logging.debug(error)
					logging.debug(' ^ in main')
